## Introduction
Ce projet a pour but d'aider les joueurs de pokémon indécis, à choisir leur pokémon de départ.
Pour ceux qui n'auraient jamais joué au jeu, sachez que le but du jeu est de devenir le meilleur dresseur de pokemon au MONDE. Pour cela vous commencerez votre aventure avec un pokemon à vos côtés. Mais ce premier pokémon, vous avez à le choisir parmi trois pokémon (communément appelés pokémons de départ, qui peuvent d'ailleurs varier d'une version à l'autre).
Votre choix à bien sûr une importance CAPITAL sur la suite de votre histoire. C'est une décision des plus COMPLEXES ET STRESSANTES à prendre. Voilà pourquoi, nous vous proposons cette simple et élégante solution à ce problème majeure dans votre vie de dresseur pokemon : un Sélécteur de Pokémon de Départ ! Habilement acronymisé en SPOD !

Le principe est simple, lorsque vous arrivez sur le site, on vous attribue un oeuf de pokémon. Vous ne savez pas quel pokémon va en éclore ! Mais c'est tout l'intérêt ! Nous choisirons pour vous le pokémon qui va en éclore, vous fournissant ainsi une réponse à la question si ardue. Nous travaillons pour le moment uniquement avec les pokémons de la première génération, mais avec un peu de chance, d'espoir et de travail, nous intégrerons peut-être les pokémons des générations suivantes. Mais n'allons pas trop vite en besogne.

## Les utilisateurs
Qui peut utiliser notre SPOD ?
Eh bien tout le monde ! Enfin, toute personne ayant accès à une connexion internet sur téléphone ou PC, et sachant lire et cliquer sur des boutons.
C'est en effet spécifiquement désigné pour des joueurs/fans de pokemons. Mais ouvert à tous, je vous assure !

## Les technologies
Symfony - PHP8
Vue.js3 - quasa
(MySQL si une BDD finit par être utilisée, mais rien n'est moins sûr)

## L'architecture
Un seul repo séparé en deux dossier backend et frontend.
Une structure MVC est utilisée

## Workflow
Branches principales :
    Une branche main (celle qui part en prod)
    une branche dev (branche où merge les features, et on test le fonctionnement)
Branches temporaires :
    type/n°issue:mots-clés (celles où on travaille les nouveaux features)