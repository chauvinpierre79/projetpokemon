<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api')]
class IndexController extends AbstractController
{
    private $pokemons = [
        ['id' => 1, 'nom' => 'Bulbizarre', 'url' => 'https://www.pokepedia.fr/images/e/ef/Bulbizarre-RFVF.png'],
        ['id' => 2, 'nom' => 'Salamèche', 'url' => 'https://www.pokepedia.fr/images/8/89/Salamèche-RFVF.png'],
        ['id' => 3, 'nom' => 'Carapuce', 'url' => 'https://www.pokepedia.fr/images/c/cc/Carapuce-RFVF.png'],

    ];

    #[Route('/test', name: 'app_index', methods: ['GET'])]
    public function index(): JsonResponse
    {
        $hello = "Hello front c'est le test de connexion";
        return new JsonResponse($hello);
    }

    /**
     * la liste des pokemons
     *
     * @return JsonResponse
     */
    #[Route('/pokemons', name: 'get_pokemon', methods: ['GET'])]
    public function getAll(): JsonResponse
    {
        return new JsonResponse($this->pokemons);
    }

    /**
     * un seul pokemon selon id
     *
     * @return JsonResponse
     */
    #[Route('/pokemon/{id}', name: 'get_by_pokemon', methods: ['GET'])]
    public function getByPokemon($id): JsonResponse
    {
        $trouve = 0;
        foreach ($this->pokemons as $item) {
            if ($item['id'] == $id) {
                $trouve = 1;
                return new JsonResponse($item);
            }
        }
        if ($trouve == 0) {
            return new JsonResponse("Pokemon n'existe pas");
        }
    }
}
