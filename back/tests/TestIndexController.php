<?php
namespace App\Tests;
require 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use App\Controller\IndexController;
use Symfony\Component\HttpFoundation\JsonResponse;

class TestIndexController extends TestCase
{
    public function testGetAll(): void
    {
        $new_index = new IndexController;
        $response = $new_index->getAll();
        $expected = new JsonResponse([
            ['id' => 1, 'nom' => 'Bulbizarre', 'url' => 'https://www.pokepedia.fr/images/e/ef/Bulbizarre-RFVF.png'],
            ['id' => 2, 'nom' => 'Salamèche', 'url' => 'https://www.pokepedia.fr/images/8/89/Salamèche-RFVF.png'],
            ['id' => 3, 'nom' => 'Carapuce', 'url' => 'https://www.pokepedia.fr/images/c/cc/Carapuce-RFVF.png'],    
    ]);
        $this->assertTrue($response == $expected);
    }
}