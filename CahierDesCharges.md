## Utilisateurs
    SuperAdmin
    JoueurEnLigne
    JoueurHorsLigne
## Contextes

**Authentification**

    - En tant que JoueurHorsLigne je veux créer un compte afin de me connecter
    - En tant que JoueurHorsLigne je veux me connecter afin d'accéder à mon compte

**Gestion de compte**

    - En tant que JourEnLigne je veux modifier mes informations perso sur mon compte
    - En tant que JoueurEnLigne je veux accéder à ma boite de Pokémon
    - En tant que JoueurEnLigne je veux voir ma liste de favoris

**StarterPokemon**

    - En tant que JoueurHorsLigne je veux choisir un starter Pokemon
    - En tant que JoueurHorsLigne je veux changer de version de starter (en ligne aussi)
    - En tant que JoueurEnLigne je veux obtenir un starter Pokemon dans ma boite pokemon

**Pokedex**

    - En tant que JoueurHorsLigne je veux accéder à la liste des pokémons existant (en ligne aussi)
    - En tant que JourEnLigne je veux ajouter des pokemons en favori



**GestUser**

    - En tant que SuperAdmin je veux voir tous les comptes afin de modérer